import Helpers from "./helpers";
import Templates from "./templates";
import Constants from "./constants";

let spelloutNumber = require('number-to-words');

export default {
    getCardValue(number)
    {
        let response = number;

        if (number === 1)
        {
            response = [
                1,
                11,
            ];
        }
        else if (number > 10)
        {
            response = 10;
        }

        return response;
    },

    getCardName(cardNumber)
    {
        let response = null;

        switch (cardNumber)
        {
            case Constants.ACE:
                response = Constants.ACE_STRING;
                break;
            case Constants.JACK:
                response = Constants.JACK_STRING;
                break;
            case Constants.QUEEN:
                response = Constants.QUEEN_STRING;
                break;
            case Constants.KING:
                response = Constants.KING_STRING;
                break;
            default:
                response = spelloutNumber.toWords(cardNumber);
                response = Helpers.capitalizeFirstLetter(response);

                break;
        }

        return response;
    },

    getCardFullName(name, suit)
    {
        return name + ' of ' + suit;
    },

    getCardImageName(name, number, suit)
    {
        let imageName = '';
        if (Constants.SPECIAL_CARDS.includes(name))
        {
            imageName += name.toLowerCase();
        }
        else
        {
            imageName += number;
        }
        imageName += '_of_' + suit.toLowerCase() + '.png';

        return imageName;
    },

    card(color, suit, number)
    {
        let card = Helpers.cloneObject(Templates.Card);

        card.color = color;
        card.suit = suit;
        card.number = number;
        card.value = this.getCardValue(number);
        card.name = this.getCardName(number);
        card.fullName = this.getCardFullName(card.name, card.suit);
        card.image += this.getCardImageName(card.name, card.number, card.suit);

        return card;
    },

    deckOfCards()
    {
        let cards = [];

        let cardsNumbers = Constants.CARDS_NUMBERS;
        let cardsColorsWithSuits = Constants.CARDS_COLORS_WITH_SUITS;

        for (let cardColor in cardsColorsWithSuits)
        {
            let cardSuits = cardsColorsWithSuits[cardColor];
            for (let cardSuit of cardSuits)
            {
                for (let cardNumber of cardsNumbers)
                {
                    let card = this.card(cardColor, cardSuit, cardNumber);
                    cards.push(card);
                }
            }
        }

        return cards;
    },
}