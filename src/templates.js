export default {
    Card: {
        color: '',
        suit: '',
        number: 0,
        value: 0,
        name: '',
        fullName: '',
        image: './assets/images/cards/png/',
    },
    Events: {
        SIMULATION_START: {
            type: 'simulation-start',
        },
        SIMULATION_END: {
            type: 'simulation-end',
            startingSum: 0,
            finishSum: 0,
            profit: 0,
            profitPercentage: 0,
            totalDealsPlayed: 0,
            dealsWon: 0,
            dealsWonPercentage: 0,
            dealsLost: 0,
            dealsLostPercentage: 0,
        },

        DEAL_START: {
            type: 'deal-start',
            dealNumber: 0,
        },
        DEAL_PLAYER: {
            type: 'deal-player',
            dealNumber: 0,
            card: {},
            hand: {},
        },
        DEAL_HOUSE: {
            type: 'deal-house',
            dealNumber: 0,
            card: {},
            hand: {},
        },
        DEAL_END: {
            type: 'deal-end',
            dealNumber: 0,
            playerHand: {},
            houseHand: {},
            playerWonSum: 0,
            playerTotalSum: 0,
            outcome: '',
            playerHandValue: 0,
            houseHandValue: 0,
        },

        PLAYER_CHOICE: {
            type: 'player-choice',
            dealNumber: 0,
            choice: '',
        },
        BLACKJACK_PLAYER: {
            type: 'blackjack-player',
            dealNumber: 0,
            hand: {},
        },
        HIT_PLAYER: {
            type: 'hit-player',
            dealNumber: 0,
            card: {},
            hand: {},
        },
        STAND_PLAYER: {
            type: 'stand-player',
            dealNumber: 0,
            hand: {},
        },
        DOUBLE_PLAYER: {
            type: 'double-player',
            dealNumber: 0,
            card: {},
            hand: {},
        },
        BUST_PLAYER: {
            type: 'bust-player',
            dealNumber: 0,
            playerHand: {},
            houseHand: {},
        },

        HOUSE_CHOICE: {
            type: 'house-choice',
            dealNumber: 0,
            choice: '',
        },
        BLACKJACK_HOUSE: {
            type: 'blackjack-house',
            dealNumber: 0,
            hand: {},
        },
        HIT_HOUSE: {
            type: 'hit-house',
            dealNumber: 0,
            card: {},
            hand: {},
        },
        STAND_HOUSE: {
            type: 'stand-house',
            dealNumber: 0,
            hand: {},
        },
        BUST_HOUSE: {
            type: 'bust-house',
            dealNumber: 0,
            playerHand: {},
            houseHand: {},
        },
    }
}