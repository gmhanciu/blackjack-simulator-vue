const NUMBER_OF_CARDS_IN_A_DECK = 52;

const RED = 'Red';
const BLACK = 'Black';

const HEARTS = 'Hearts';
const DIAMONDS = 'Diamonds';
const SPADES = 'Spades';
const CLUBS = 'Clubs';

const ACE_STRING = 'Ace';
const JACK_STRING = 'Jack';
const QUEEN_STRING = 'Queen';
const KING_STRING = 'King';

export default Object.freeze({
    NUMBER_OF_CARDS_IN_A_DECK: NUMBER_OF_CARDS_IN_A_DECK,
    NUMBER_OF_CARD_COLORS: 4,
    NUMBER_OF_CARDS_WITH_SAME_COLOR: 13,

    /*
     * soft 17 => the hand contains an ace that can be counted as 1 to prevent busting
     * hard 17 => the hand contains an ace that is counted as 11 and it can't prevent busting anymore
     *
     * for a house soft 17, the maximum number of cards that would be needed to achieve this is 12
     * eg: A,A,A,A,A,A,6,A,A,A,A
     * for a house hard 17, the maximum number of cards that would be needed to achieve this is 13
     * eg: A,A,A,A,A,A,A,5,A,A,A,A
     *
     * so the maximum number of cards that can be done per deal for both player and house is 13 + x
     * where x could vary based on the player's choices (rough it up to 7 cards at most)
     * */
    NUMBER_OF_MAXIMUM_CARDS_PER_DEAL: 20,

    /*
     * after the cards have been shuffled and the player cut the decks, the last 60 to 75 cards
     * will not be played in order to make it harder for professional card counters to operate effectively
     *
     * for easier code management, rounded it up to a full deck of cards
     * */
    NUMBER_OF_CARDS_THAT_WONT_BE_PLAYED: NUMBER_OF_CARDS_IN_A_DECK,

    /*
    * Cards colors
    * */
    RED: RED,
    BLACK: BLACK,
    CARDS_COLORS: [
        RED,
        BLACK,
    ],

    /*
    * Cards Symbols
    * */
    HEARTS: HEARTS,
    CLUBS: CLUBS,
    SPADES: SPADES,
    DIAMONDS: DIAMONDS,
    CARDS_SUITS: [
        HEARTS,
        CLUBS,
        SPADES,
        DIAMONDS,
    ],

    /*
    * Special Cards Numbers
    * */
    ACE: 1,
    JACK: 12,
    QUEEN: 13,
    KING: 14,

    /*
    * Special Cards Names
    * */
    ACE_STRING: ACE_STRING,
    JACK_STRING: JACK_STRING,
    QUEEN_STRING: QUEEN_STRING,
    KING_STRING: KING_STRING,

    /*
    * Special Cards Values
    * */
    ACE_VALUE: [
        1,
        11,
    ],
    JACK_VALUE: 10,
    QUEEN_VALUE: 10,
    KING_VALUE: 10,

    SPECIAL_CARDS: [
        ACE_STRING,
        JACK_STRING,
        QUEEN_STRING,
        KING_STRING
    ],

    CARDS_NUMBERS: [
        1,
        2,
        3,
        4,
        5,
        6,
        7,
        8,
        9,
        10,
        12,
        13,
        14,
    ],

    CARDS_COLORS_WITH_SUITS: {
        [RED]: [
            HEARTS,
            DIAMONDS,
        ],
        [BLACK]: [
            SPADES,
            CLUBS,
        ],
    },

    BUST: 21,
    BLACKJACK: 21,

    SOFT_HAND: 'Soft',
    HARD_HAND: 'Hard',

    BET_RATIO: 1,
    LOSE_RATIO: -1,
    PUSH_RATIO: 0,
    DOUBLE_RATIO: 3,
    BLACKJACK_RATIO: 1.5,

    OUTCOME_WIN: 'Won',
    OUTCOME_PUSH: 'Push',
    OUTCOME_LOSE: 'Lost',
});
