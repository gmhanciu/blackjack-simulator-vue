export default {
    SIMULATION_START: 'simulation-start',
    SIMULATION_END: 'simulation-end',

    DEAL_START: 'deal-start',
    DEAL_HOUSE: 'deal-house',
    DEAL_PLAYER: 'deal-player',
    DEAL_END: 'deal-end',

    PLAYER_CHOICE: 'player-choice',
    BLACKJACK_PLAYER: 'blackjack-player',
    HIT_PLAYER: 'hit-player',
    DOUBLE_PLAYER: 'double-player',
    STAND_PLAYER: 'stand-player',
    SPLIT_PLAYER: 'split-player',
    BUST_PLAYER: 'bust-player',

    HOUSE_CHOICE: 'house-choice',
    BLACKJACK_HOUSE: 'blackjack-house',
    HIT_HOUSE: 'hit-house',
    STAND_HOUSE: 'stand-house',
    BUST_HOUSE: 'bust-house',
}