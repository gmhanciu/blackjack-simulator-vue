import 'bootstrap';
import 'bootstrap/dist/css/bootstrap.min.css';

import Vue from 'vue';
import store from './store/index';

import App from './App';

import constants from './constants';
Vue.prototype.$Constants = constants;

import helpers from './helpers';
Vue.prototype.$Helpers = helpers;

import cards from './helpers';
Vue.prototype.$Cards = cards;

import events from './events';
Vue.prototype.$Events = events;


/* eslint-disable no-new */
new Vue({
    store,
    render: h => h(App)
}).$mount('#app');

