export default {

    capitalizeFirstLetter(string) {
        if (typeof string !== 'string') return '';
        return string.charAt(0).toUpperCase() + string.slice(1);
    },

    cloneObject(o)
    {
        let out, v, key;
        out = Array.isArray(o) ? [] : {};
        for (key in o) {
            v = o[key];
            out[key] = (typeof v === "object" && v !== null) ? this.cloneObject(v) : v;
        }
        return out;
    },

    /**
     * Randomize array element order in-place.
     * Using Durstenfeld shuffle algorithm.
     */
    shuffleArray(array)
    {
        for (let i = array.length - 1; i > 0; i--)
        {
            const j = Math.floor(Math.random() * (i + 1));
            [array[i], array[j]] = [array[j], array[i]];
        }

        return array;
    },

    toCamelCaseFirstUC (str)
    {
        return str.replace(/(?:^\w|[A-Z]|\b\w)/g, function(word, index)
            {
                return /*index == 0 ? word.toLowerCase() :*/ word.toUpperCase();
            }).replace(/\s+|-/g, '');
    },
    toCamelCase (str)
    {
        return str.replace(/(?:^\w|[A-Z]|\b\w)/g, function(word, index)
            {
                return index == 0 ? word.toLowerCase() : word.toUpperCase();
            }).replace(/\s+|-/g, '');
    },

}