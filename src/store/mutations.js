import Cards from './../cards';
import Constants from './../constants';
import Helpers from './../helpers';
import Templates from './../templates';
import Events from './../events';
import InitialState from './state';


export default {
    resetStore: (state) => {
        Object.assign(state, InitialState());
    },
    setNumberOfSessions(state, payload)
    {
        if (payload === "")
        {
            payload = 0;
        }
        state.numberOfSessions = payload;
    },
    setNumberOfDecks(state, payload)
    {
        if (payload === "")
        {
            payload = 0;
        }
        state.numberOfDecks = payload;
    },
    setStartingSum(state, payload)
    {
        if (payload === "")
        {
            payload = 0;
        }
        state.startingSum = payload;
    },
    setStrategy(state, payload)
    {
        state.strategy = payload;
        state.strategyFunction = require('../strategies/' + payload).strategy;
    },

    generateCards(state, payload)
    {
        let numberOfDecks;

        numberOfDecks = state.numberOfDecks * state.numberOfSessions;
        state.numberOfDecksToBePlayed = numberOfDecks;
        //extra deck that won't be played
        numberOfDecks++;

        for (let decksCount = 0; decksCount < numberOfDecks; decksCount++)
        {
            let deckOfCards = Cards.deckOfCards();
            state.decksOfCards.push(deckOfCards);
        }

        state.cards = state.decksOfCards.flat();
        state.cards = Helpers.shuffleArray(state.cards);
    },
    simulationStart(state, payload)
    {
        state.unitsSumRatio = state.startingSum / state.units;

        let event = Helpers.cloneObject(Templates.Events.SIMULATION_START);
        state.events.push(event);
    },
    simulationEnd(state, payload)
    {
        state.finishSum = state.units * state.unitsSumRatio;

        let event = Helpers.cloneObject(Templates.Events.SIMULATION_END);
        event.finishSum = state.finishSum;
        event.startingSum = state.startingSum;
        event.profit = state.finishSum - state.startingSum;
        //% Decrease = Decrease / Original Number * 100
        event.profitPercentage = Math.ceil(event.profit / event.startingSum * 100);
        event.totalDealsPlayed = state.currentDealNumber;
        event.dealsWon = state.dealsWon;
        event.dealsWonPercentage = Math.ceil(event.dealsWon * 100 / event.totalDealsPlayed);
        event.dealsLost = state.dealsLost;
        event.dealsLostPercentage = Math.ceil(event.dealsLost * 100 / event.totalDealsPlayed);
        state.events.push(event);
    },
    dealStart(state, payload)
    {
        /*
        * add event of dealing cards (because the cards have already been shuffled)
        * */
        state.currentDealNumber++;

        let event = Helpers.cloneObject(Templates.Events.DEAL_START);
        event.dealNumber = state.currentDealNumber;

        state.events.push(event);
    },
    dealPlayer: (state) => {
        let card = state.cards.pop();
        state.player.hand.push(card);

        let event = Helpers.cloneObject(Templates.Events.DEAL_PLAYER);
        event.dealNumber = state.currentDealNumber;
        event.card = card;
        event.hand = Helpers.cloneObject(state.player.hand);

        state.events.push(event);
    },
    dealHouse: (state) => {
        let card = state.cards.pop();
        state.house.hand.push(card);

        let event = Helpers.cloneObject(Templates.Events.DEAL_HOUSE);
        event.dealNumber = state.currentDealNumber;
        event.card = card;
        event.hand = Helpers.cloneObject(state.house.hand);

        state.events.push(event);
    },
    dealEnd(state, payload)
    {
        // deal outcome sum
        let dealOutcomeSum = payload.payRatio * state.betPerHand;
        state.units += dealOutcomeSum;

        //this is mostly for display because
        //in online professional blackjack
        //when you bet you're money are being subtracted from the balance
        //when you win, the sum displayed is the won sum (ratio to the bet) + the bet
        //but since we don't subtract the initial bet, we don't add it to the outcome either
        let dealOutcomeFullSum = dealOutcomeSum;

        if (dealOutcomeSum >= 0)
        {
            dealOutcomeFullSum += state.betPerHand;
        }

        let event = Helpers.cloneObject(Templates.Events.DEAL_END);
        event.dealNumber = state.currentDealNumber;
        event.playerHand = Helpers.cloneObject(state.player.hand);
        event.houseHand = Helpers.cloneObject(state.house.hand);
        event.playerWonSum = dealOutcomeFullSum * state.unitsSumRatio;
        event.outcome = payload.outcome;
        event.playerHandValue = payload.playerHandValue;
        event.houseHandValue = payload.houseHandValue;

        if (event.playerWonSum > 0)
        {
            state.dealsWon++;
        }
        else
        {
            state.dealsLost++;
        }

        state.events.push(event);
        state.events = state.events.filter((event, eventKey) => (event.type === Events.DEAL_END));

        state.cardsPlayedSoFar += event.playerHand.length + event.houseHand.length;
        state.decksPlayedSoFar = state.cardsPlayedSoFar / Constants.NUMBER_OF_CARDS_IN_A_DECK;
        state.decksPlayedSoFar = Math.floor(state.decksPlayedSoFar);

        // state.deals.push(Helpers.cloneObject(state.events));
        // state.events = [];

        /*
        * clear for next deal
        * */

        state.house.hand = [];
        state.player.hand = [];

    },
    playerChoice(state, payload)
    {
        /*
        * here we are sure the player didn't bust
        * so we go on with the logic
        * */
        let playerHand = state.player.hand;
        let houseHand = state.house.hand;

        /*
        * Player Choices
        *
        * Double up (pull 1 card and double the bet)
        * Split (if pair)
        * Hit
        * Stand
        * ** NO SURRENDER **
        * */

        let nextAction = state.strategyFunction(playerHand, houseHand);

        let event = Helpers.cloneObject(Templates.Events.PLAYER_CHOICE);
        event.dealNumber = state.currentDealNumber;
        event.choice = nextAction;

        if (playerHand.length === 2 && payload === Constants.BLACKJACK)
        {
            event.choice = Events.BLACKJACK_PLAYER;
        }

        state.events.push(event);
    },
    blackjackPlayer: (state) => {
        let event = Helpers.cloneObject(Templates.Events.BLACKJACK_PLAYER);
        state.events.push(event);
    },
    hitPlayer: (state) => {
        let card = state.cards.pop();
        state.player.hand.push(card);

        let event = Helpers.cloneObject(Templates.Events.HIT_PLAYER);
        event.dealNumber = state.currentDealNumber;
        event.card = card;
        event.hand = Helpers.cloneObject(state.player.hand);

        state.events.push(event);
    },
    standPlayer: (state) => {
        let event = Helpers.cloneObject(Templates.Events.STAND_PLAYER);
        event.dealNumber = state.currentDealNumber;
        event.hand = Helpers.cloneObject(state.player.hand);

        state.events.push(event);
    },
    doublePlayer: (state) => {
        let card = state.cards.pop();
        state.player.hand.push(card);

        let event = Helpers.cloneObject(Templates.Events.DOUBLE_PLAYER);
        event.dealNumber = state.currentDealNumber;
        event.card = card;
        event.hand = Helpers.cloneObject(state.player.hand);

        state.events.push(event);
    },
    bustPlayer: (state) => {
        let event = Helpers.cloneObject(Templates.Events.BUST_PLAYER);
        event.dealNumber = state.currentDealNumber;
        event.playerHand = Helpers.cloneObject(state.player.hand);
        event.houseHand = Helpers.cloneObject(state.house.hand);

        state.events.push(event);
    },
    houseChoice(state, payload)
    {
        let houseHandDetails = payload.handDetails;
        let houseHand = payload.hand;
        let pointsInHand = houseHandDetails.value;

        let nextAction = null;

        if (pointsInHand >= 17 && houseHandDetails.type === Constants.HARD_HAND)
        {
            nextAction = Events.STAND_HOUSE;
        }
        else if (pointsInHand === Constants.BLACKJACK && houseHand.length === 2)
        {
            nextAction = Events.BLACKJACK_HOUSE;
        }
        else
        {
            nextAction = Events.HIT_HOUSE;
        }

        let event = Helpers.cloneObject(Templates.Events.HOUSE_CHOICE);
        event.dealNumber = state.currentDealNumber;
        event.choice = nextAction;

        state.events.push(event);
    },
    blackjackHouse: (state) => {
        let event = Helpers.cloneObject(Templates.Events.BLACKJACK_HOUSE);
        state.events.push(event);
    },
    hitHouse: (state) => {
        let card = state.cards.pop();
        state.house.hand.push(card);

        let event = Helpers.cloneObject(Templates.Events.HIT_HOUSE);
        event.dealNumber = state.currentDealNumber;
        event.card = card;
        event.hand = Helpers.cloneObject(state.house.hand);

        state.events.push(event);
    },
    standHouse: (state) => {
        let event = Helpers.cloneObject(Templates.Events.STAND_HOUSE);
        event.dealNumber = state.currentDealNumber;
        event.hand = Helpers.cloneObject(state.house.hand);

        state.events.push(event);
    },
    bustHouse: (state) => {
        let event = Helpers.cloneObject(Templates.Events.BUST_HOUSE);
        event.dealNumber = state.currentDealNumber;
        event.playerHand = Helpers.cloneObject(state.player.hand);
        event.houseHand = Helpers.cloneObject(state.house.hand);

        state.events.push(event);
    },
}