import Events from "../events";
import Constants from '../constants';
import Helpers from '../helpers';

export default {
    getNumberOfSessions: (state) => {
        return state.numberOfSessions;
    },
    getNumberOfDecks: (state) => {
        return state.numberOfDecks;
    },
    getStrategy: (state) => {
        return state.strategy;
    },
    getStartingSum: (state) => {
        return state.startingSum;
    },
    getSimulationEvents: (state) => {
        return state.events;
    },
    getCurrentDealNumber: (state) => {
        return state.currentDealNumber;
    },
    getDealNumberByEvent: (state) => (payload) => {
        return state.events[payload].dealNumber;
    },
    getLastEvent: (state) => {
        // if (state.events.length === 0 && state.deals.length > 0)
        // {
        //     let lastDeal = state.deals[state.deals.length - 1];
        //     return lastDeal[lastDeal.length - 1];
        // }
        // else
        // {
            return state.events[state.events.length - 1];
        // }
    },
    getEventByID: (state) => payload => {
        return state.events[payload];
    },
    getHandDetails: (state) => payload => {
        let response = {
            value: 0,
            type: Constants.HARD_HAND,
        };

        //sort cards so that the aces will always be the last
        //because we need to process the aces compared to the hard value of the hand
        // let hand = payload.slice(0).sort((a, b) => (a.value.constructor === Array) ? -1 : ((b.value.constructor === Array) ? -1 : ((typeof a === 'number') ? 1 : ((typeof b === 'number') ? 1 : 0))));
        // let hand = payload.slice(0).sort((a, b) => ( (a.value.constructor === Array && !Array.isArray(b)) || (!Array.isArray(a) && b.value.constructor === Array) ) ? 1 : 0);
        let hand = payload.slice(0).sort(function (a, b) {
            if (a.value.constructor === Array && typeof b === 'number')
            {
                return 1;
            }
            else if (typeof a === 'number' && b.value.constructor === Array)
            {
                return 1;
            }
            else
            {
                return -1;
            }
        });

        for (let card of hand)
        {
            let cloneCard = Helpers.cloneObject(card);
            let cardValue = cloneCard.value;

            /*
            * this can only happen if we encounter an 'Ace' cause it can be considered either 1 or 11
            * leading to either a 'soft' or a 'hard' hand
            * */
            if (cardValue.constructor === Array)
            {
                let possibleValues = [];

                for (let tempCardValue of cardValue)
                {
                    //if new overall value doesn't lead to over BUST, consider the combination valid
                    if (response.value + tempCardValue <= Constants.BUST)
                    {
                        possibleValues.push(tempCardValue);
                    }
                }

                //sort ascending and pick the last value
                //by sorting ascending we get [1, 11], last one being 11
                //if both Ace values do not lead to BUST, we pick the highest one
                //to benefit from a 'soft' hand (later in case the player would bust because
                //the ace is 11, the ace could become 1 and keep the player in game)

                //extra check if the possibleValues array is empty
                //because apparently there are cases when this is empty (like the player busted0
                //so aces won't ever check the condition

                if (possibleValues.length === 0)
                {
                    continue;
                }

                cardValue = possibleValues.sort().pop();

                // if any ace is 11, the hand is still soft
                if (cardValue === 11)
                {
                    response.type = Constants.SOFT_HAND;
                }
            }

            response.value += cardValue;
        }

        return response;
    },
    checkIfHandCanBeSplit: (state) => payload => {
        let response = false;

        let firstCard = payload[0];
        let secondCard = payload[1];

        if (firstCard.name === secondCard.name && payload.length === 2)
        {
            response = true;
        }

        return response;
    },
    getNextEvent: (state, getters) => {
        const lastEvent = getters.getLastEvent;

        if (lastEvent === undefined)
        {
            return null;
        }

        if (lastEvent.type === Events.SIMULATION_START)
        {
            return Events.DEAL_START;
        }
        else if (lastEvent.type === Events.DEAL_START)
        {
            return Events.DEAL_PLAYER;
        }
        else if (lastEvent.type === Events.DEAL_PLAYER)
        {
            return Events.DEAL_HOUSE;
        }
        else if (lastEvent.type === Events.DEAL_HOUSE && state.player.hand.length === 1)
        {
            return Events.DEAL_PLAYER;
        }
        else if (lastEvent.type === Events.DEAL_PLAYER && state.house.hand.length === 1)
        {
            return Events.DEAL_HOUSE;
        }
        else if (lastEvent.type === Events.DEAL_HOUSE && state.player.hand.length === 2)
        {
            return Events.PLAYER_CHOICE;
        }
        else if (lastEvent.type === Events.PLAYER_CHOICE || lastEvent.type === Events.HOUSE_CHOICE)
        {
            return lastEvent.choice;
        }
        else if ([Events.SPLIT_PLAYER, Events.HIT_PLAYER].includes(lastEvent.type))
        {
            return Events.PLAYER_CHOICE;
        }
        else if (lastEvent.type === Events.DOUBLE_PLAYER)
        {
            return Events.STAND_PLAYER;
        }
        else if (lastEvent.type === Events.STAND_PLAYER)
        {
            return Events.HOUSE_CHOICE;
        }
        else if (lastEvent.type === Events.HIT_HOUSE)
        {
            return Events.HOUSE_CHOICE;
        }
        else if (lastEvent.type === Events.STAND_HOUSE)
        {
            return Events.DEAL_END;
        }
        else if (lastEvent.type === Events.BUST_PLAYER || lastEvent.type === Events.BUST_HOUSE)
        {
            return Events.DEAL_END;
        }
        else if (lastEvent.type === Events.BLACKJACK_PLAYER)
        {
            return Events.HOUSE_CHOICE;
        }
        else if (lastEvent.type === Events.BLACKJACK_HOUSE)
        {
            return Events.DEAL_END;
        }
        else if (lastEvent.type === Events.DEAL_END)
        {
            if (state.numberOfDecksToBePlayed === state.decksPlayedSoFar)
            {
                return Events.SIMULATION_END;
            }
            else
            {
                return Events.DEAL_START;
            }
        }
    },

    getDealOutcome: (state, getters) => payload => {
        let response = {
            outcome: '',
            payRatio: Constants.BET_RATIO,
        };

        let playerHand = payload.playerHand;
        let houseHand = payload.houseHand;

        let playerHandDetails = getters.getHandDetails(playerHand);
        let houseHandDetails = getters.getHandDetails(houseHand);

        //push cases
        if (playerHandDetails.value > Constants.BUST && houseHandDetails.value > Constants.BUST)
        {
            response.outcome = Constants.OUTCOME_PUSH;
            response.payRatio = Constants.PUSH_RATIO;
        }
        else if (playerHandDetails.value === houseHandDetails.value)
        {
            response.outcome = Constants.OUTCOME_PUSH;
            response.payRatio = Constants.PUSH_RATIO;
        }
        //lose cases
        else if (playerHandDetails.value > Constants.BUST && houseHandDetails.value <= Constants.BUST)
        {
            response.outcome = Constants.OUTCOME_LOSE;
            response.payRatio = Constants.LOSE_RATIO;
        }
        else if (playerHandDetails.value < houseHandDetails.value && houseHandDetails.value <= Constants.BLACKJACK)
        {
            response.outcome = Constants.OUTCOME_LOSE;
            response.payRatio = Constants.LOSE_RATIO;
        }
        //win cases
        else if (playerHandDetails.value <= Constants.BLACKJACK && playerHandDetails.value > houseHandDetails.value)
        {
            let playerBlackjackEvent = state.events.filter((event, key) => event.type === Events.BLACKJACK_PLAYER);
            let playerDoubleEvent = state.events.filter((event, key) => event.type === Events.DOUBLE_PLAYER);

            if (playerBlackjackEvent.length > 0)
            {
                response.payRatio = Constants.BLACKJACK_RATIO;
            }
            else if (playerDoubleEvent.length > 0)
            {
                response.payRatio = Constants.DOUBLE_RATIO;
            }

            response.outcome = Constants.OUTCOME_WIN;
        }
        else if (playerHandDetails.value <= Constants.BLACKJACK && houseHandDetails.value > Constants.BUST)
        {
            response.outcome = Constants.OUTCOME_WIN;
        }

        return response;
    },
}