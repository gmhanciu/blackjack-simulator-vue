import Constants from './../constants';
import Events from './../events';
import Helpers from "../helpers";

export default {
    resetStore({state, commit, rootState})
    {
        return new Promise(function (resolve, reject) {
            commit('resetStore');
        });
    },
    setNumberOfSessions({state, commit, rootState}, payload)
    {
        commit('setNumberOfSessions', payload);
    },
    setNumberOfDecks({state, commit, rootState}, payload)
    {
        commit('setNumberOfDecks', payload);
    },
    setStrategy({state, commit, rootState}, payload)
    {
        commit('setStrategy', payload);
    },
    setStartingSum({state, commit, rootState}, payload)
    {
        commit('setStartingSum', payload);
    },
    generateCards ({state, commit, rootState})
    {
        return new Promise(function (resolve, reject) {
            commit('generateCards');
            resolve();
        });
    },
    simulationStart({state, commit, rootState, getters, dispatch})
    {
        commit('simulationStart');

        let nextEvent;

        while (nextEvent !== this._vm.$Helpers.toCamelCase(Events.SIMULATION_END))
        {
            nextEvent = getters.getNextEvent;

            nextEvent = this._vm.$Helpers.toCamelCase(nextEvent);
            dispatch(nextEvent);
        }
    },
    simulationEnd({state, commit, rootState, getters, dispatch})
    {
        return new Promise(function (resolve, reject) {
            commit('simulationEnd');
            resolve();
        });
    },
    dealStart({state, commit, rootState})
    {
        return new Promise(function (resolve, reject) {
            commit('dealStart');
            resolve();
        });
    },
    dealEnd({state, commit, rootState, getters})
    {
        return new Promise(function (resolve, reject) {
            let payload = {
                playerHand: Helpers.cloneObject(state.player.hand),
                houseHand: Helpers.cloneObject(state.house.hand),
            };

            let dealOutcome = getters.getDealOutcome(payload);
            dealOutcome.playerHandValue = getters.getHandDetails(payload.playerHand).value;
            dealOutcome.houseHandValue = getters.getHandDetails(payload.houseHand).value;

            commit('dealEnd', dealOutcome);
            resolve();
        });
    },
    dealPlayer({state, commit, rootState})
    {
        return new Promise(function (resolve, reject) {
            commit('dealPlayer');
            resolve();
        })
    },
    dealHouse({state, commit, rootState})
    {
        return new Promise(function (resolve, reject) {
            commit('dealHouse');
            resolve();
        })
    },
    playerChoice({state, commit, rootState, getters})
    {
        return new Promise(function (resolve, reject) {
            let handDetails = getters.getHandDetails(state.player.hand);
            let pointsInHand = handDetails.value;

            if (pointsInHand > Constants.BUST)
            {
                commit('bustPlayer');
            }
            else
            {
                commit('playerChoice', pointsInHand);
            }
            resolve();
        })
    },
    blackjackPlayer({state, commit, rootState, getters})
    {
        return new Promise(function (resolve, reject) {
            commit('blackjackPlayer');
            resolve();
        });
    },
    hitPlayer({state, commit, rootState, getters})
    {
        return new Promise(function (resolve, reject) {
            commit('hitPlayer');
            resolve();
        });
    },
    standPlayer({state, commit, rootState, getters})
    {
        return new Promise(function (resolve, reject) {
            commit('standPlayer');
            resolve();
        });
    },
    doublePlayer({state, commit, rootState, getters})
    {
        return new Promise(function (resolve, reject) {
            commit('doublePlayer');
            resolve();
        });
    },
    houseChoice({state, commit, rootState, getters})
    {
        return new Promise(function (resolve, reject) {
            let payload = {
                handDetails: getters.getHandDetails(state.house.hand),
                hand: state.house.hand,
            };
            let pointsInHand = payload.handDetails.value;

            if (pointsInHand > Constants.BUST)
            {
                commit('bustHouse');
            }
            else
            {
                commit('houseChoice', payload);
            }
            resolve();
        })
    },
    blackjackHouse({state, commit, rootState, getters})
    {
        return new Promise(function (resolve, reject) {
            commit('blackjackHouse');
            resolve();
        });
    },
    hitHouse({state, commit, rootState, getters})
    {
        return new Promise(function (resolve, reject) {
            commit('hitHouse');
            resolve();
        });
    },
    standHouse({state, commit, rootState, getters})
    {
        return new Promise(function (resolve, reject) {
            commit('standHouse');
            resolve();
        });
    },
}