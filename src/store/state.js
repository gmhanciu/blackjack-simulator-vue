const initialState = () => {
    return {
        numberOfDecks: 4,
        numberOfSessions: 1,
        numberOfDecksToBePlayed: 0,
        startingSum: 1000,
        finishSum: 0,
        units: 100000,
        betPerHand: 1000,
        unitsSumRatio: 0,
        strategy: '+4_decks_das_h17',
        strategyFunction: require('../strategies/+4_decks_das_h17').strategy,
        decksOfCards: [],
        cards: [],
        events: [],
        house: {
            hand: [],
        },
        player: {
            hand: [],
        },
        currentDealNumber: 0,
        decksPlayedSoFar: 0,
        cardsPlayedSoFar: 0,
        dealsWon: 0,
        dealsLost: 0,
        nextEvent: '',
        deals: [],
    }
};

export default initialState;

// export default {
//     numberOfDecks: 4,
//     numberOfSessions: 1,
//     numberOfDecksToBePlayed: 0,
//     startingSum: 1000,
//     strategy: '+4_decks_das_h17',
//     strategyFunction: require('../strategies/+4_decks_das_h17').strategy,
//     decksOfCards: [],
//     cards: [],
//     events: [],
//     house: {
//         hand: [],
//     },
//     player: {
//         hand: [],
//     },
//     currentDealNumber: 0,
//     decksPlayedSoFar: 0,
//     cardsPlayedSoFar: 0,
//     nextEvent: '',
//     deals: [],
// }