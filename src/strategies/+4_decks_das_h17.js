import Events from '../events';
import Constants from '../constants';
import Store from '../store/index'

function strategy(playerHand, houseHand)
{
    let nextMove = null;

    let playerHandDetails = Store.getters.getHandDetails(playerHand);

    // let handCanBeSplit = Store.getters.checkIfHandCanBeSplit(playerHand);

    // if (handCanBeSplit) {
    //     nextMove = splitStrategy(playerHandDetails, houseHand);
    // }
    // else {
        switch (playerHandDetails.type) {
            case Constants.SOFT_HAND:
                nextMove = softHandStrategy(playerHandDetails, houseHand);
                break;
            case Constants.HARD_HAND:
                nextMove = hardHandStrategy(playerHandDetails, houseHand);
                break;
        }
    // }

    return nextMove;
}

function hardHandStrategy(playerHand, houseHand)
{
    let nextMove = null;

    let houseFaceUpCardValue = houseHand[0].value;

    switch (playerHand.value)
    {
        case 4:
        case 5:
        case 6:
        case 7:
        case 8:
            nextMove = Events.HIT_PLAYER;
            break;
        case 9:
            switch (houseFaceUpCardValue)
            {
                /*
                * we can check for ace as 11 because
                * if the house gets an ace as their face card
                * this means that it will be always considered a soft hand
                * no matter what the second card is
                * */
                case 2:
                case 7:
                case 8:
                case 9:
                case 10:
                case 11:
                    nextMove = Events.HIT_PLAYER;
                    break;
                default:
                    nextMove = Events.DOUBLE_PLAYER;
                    break;
            }
            break;
        case 10:
            switch (houseFaceUpCardValue)
            {
                case 10:
                case 11:
                    nextMove = Events.HIT_PLAYER;
                    break;
                default:
                    nextMove = Events.DOUBLE_PLAYER;
                    break;
            }
            break;
        case 11:
            nextMove = Events.DOUBLE_PLAYER;
            break;
        case 12:
            switch (houseFaceUpCardValue)
            {
                case 4:
                case 5:
                case 6:
                    nextMove = Events.STAND_PLAYER;
                    break;
                default:
                    nextMove = Events.HIT_PLAYER;
                    break;
            }
            break;
        case 13:
        case 14:
            switch (houseFaceUpCardValue)
            {
                case 2:
                case 3:
                case 4:
                case 5:
                case 6:
                    nextMove = Events.STAND_PLAYER;
                    break;
                default:
                    nextMove = Events.HIT_PLAYER;
                    break;
            }
            break;
        case 15:
            switch (houseFaceUpCardValue)
            {
                case 7:
                case 8:
                case 9:
                    nextMove = Events.HIT_PLAYER;
                    break;
                //suh
                case 10:
                case 11:
                    nextMove = Events.HIT_PLAYER;
                    break;
                default:
                    nextMove = Events.STAND_PLAYER;
                    break;
            }
            break;
        case 16:
            switch (houseFaceUpCardValue)
            {
                case 7:
                case 8:
                    nextMove = Events.HIT_PLAYER;
                    break;
                //suh
                case 9:
                case 10:
                case 11:
                    nextMove = Events.HIT_PLAYER;
                    break;
                default:
                    nextMove = Events.STAND_PLAYER;
                    break;
            }
            break;
        case 17:
            switch (houseFaceUpCardValue)
            {
                //sus
                case 11:
                    nextMove = Events.STAND_PLAYER;
                    break;
                default:
                    nextMove = Events.STAND_PLAYER;
                    break;
            }
            break;
        case 18:
        case 19:
        case 20:
        case 21:
            nextMove = Events.STAND_PLAYER;
            break;
    }
    return nextMove;
}

function softHandStrategy(playerHand, houseHand)
{
    let nextMove = null;

    let houseFaceUpCardValue = houseHand[0].value;

    switch (playerHand.value)
    {
        case 12:
        case 13:
        case 14:
            switch (houseFaceUpCardValue)
            {
                case 5:
                case 6:
                    nextMove = Events.DOUBLE_PLAYER;
                    break;
                default:
                    nextMove = Events.HIT_PLAYER;
                    break;
            }
            break;
        case 15:
        case 16:
            switch (houseFaceUpCardValue)
            {
                case 4:
                case 5:
                case 6:
                    nextMove = Events.DOUBLE_PLAYER;
                    break;
                default:
                    nextMove = Events.HIT_PLAYER;
                    break;
            }
            break;
        case 17:
            switch (houseFaceUpCardValue)
            {
                case 3:
                case 4:
                case 5:
                case 6:
                    nextMove = Events.DOUBLE_PLAYER;
                    break;
                default:
                    nextMove = Events.HIT_PLAYER;
                    break;
            }
            break;
        case 18:
            switch (houseFaceUpCardValue)
            {
                case 2:
                case 3:
                case 4:
                case 5:
                case 6:
                    nextMove = Events.DOUBLE_PLAYER;
                    break;
                case 7:
                case 8:
                    nextMove = Events.STAND_PLAYER;
                    break;
                default:
                    nextMove = Events.HIT_PLAYER;
                    break;
            }
            break;
        case 19:
            switch (houseFaceUpCardValue)
            {
                case 6:
                    nextMove = Events.DOUBLE_PLAYER;
                    break;
                default:
                    nextMove = Events.STAND_PLAYER;
                    break;
            }
            break;
        case 20:
        case 21:
            nextMove = Events.STAND_PLAYER;
            break;
    }

    return nextMove;
}

function splitStrategy(playerHand, houseHand)
{
    let nextMove = null;

    let houseFaceUpCardValue = houseHand[0].value;

    switch (playerHand.value)
    {
        /*
        * here we can use the total hand value to determine
        * which kind of pair we have
        * eg: a 2-2 pair has the total value of 4
        * 3-3 pair = 6, etc
        * */
        case 4:
        case 6:
            switch (houseFaceUpCardValue)
            {
                case 2:
                case 3:
                case 4:
                case 5:
                case 6:
                case 7:
                    nextMove = Events.SPLIT_PLAYER;
                    break;
                default:
                    nextMove = Events.HIT_PLAYER;
                    break;
            }
            break;
        case 8:
            switch (houseFaceUpCardValue)
            {
                case 5:
                case 6:
                    nextMove = Events.SPLIT_PLAYER;
                    break;
                default:
                    nextMove = Events.HIT_PLAYER;
                    break;
            }
            break;
        case 10:
            switch (houseFaceUpCardValue)
            {
                case 10:
                case 11:
                    nextMove = Events.HIT_PLAYER;
                    break;
                default:
                    nextMove = Events.SPLIT_PLAYER;
                    break;
            }
            break;
        /*
        * this is a special case because we can hit total 12 in 2 cases
        * 6-6
        * Ace-Ace
        *
        * Ace-Ace will be a soft hand because an ace will be 11 and the other one 1
        * if both aces would've been 11, the player would've bust
        *
        * 6-6 is a hard hand because there are no aces involved
        *
        * So in order to distinguish those situations, we can use the hand type (soft or hard)
        * */
        case 12:
            switch (playerHand.type)
            {
                // Ace-Ace case
                case Constants.SOFT_HAND:
                    nextMove = Events.SPLIT_PLAYER;
                    break;
                // 6-6 case
                case Constants.HARD_HAND:
                    switch (houseFaceUpCardValue)
                    {
                        case 2:
                        case 3:
                        case 4:
                        case 5:
                        case 6:
                            nextMove = Events.SPLIT_PLAYER;
                            break;
                        default:
                            nextMove = Events.HIT_PLAYER;
                            break;
                    }
                    break;
            }
            break;
        case 14:
            switch (houseFaceUpCardValue)
            {
                case 2:
                case 3:
                case 4:
                case 5:
                case 6:
                case 7:
                    nextMove = Events.SPLIT_PLAYER;
                    break;
                default:
                    nextMove = Events.HIT_PLAYER;
                    break;
            }
            break;
        case 16:
            switch (houseFaceUpCardValue)
            {
                //sup = surrender if allowed, otherwise hit
                case 11:
                    nextMove = Events.SPLIT_PLAYER;
                    break;
                default:
                    nextMove = Events.SPLIT_PLAYER;
                    break;
            }
            break;
        case 18:
            switch (houseFaceUpCardValue)
            {
                //sup = surrender if allowed, otherwise hit
                case 7:
                case 10:
                case 11:
                    nextMove = Events.STAND_PLAYER;
                    break;
                default:
                    nextMove = Events.SPLIT_PLAYER;
                    break;
            }
            break;
        case 20:
            nextMove = Events.STAND_PLAYER;
            break;
    }

    return nextMove;
}

export { strategy }