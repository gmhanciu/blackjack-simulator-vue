import Events from '../events';
import Constants from '../constants';
import Store from '../store/index'

function strategy(playerHand, houseHand)
{
    let nextMove = null;

    let playerHandDetails = Store.getters.getHandDetails(playerHand);

    switch (playerHandDetails.type) {
        case Constants.SOFT_HAND:
            nextMove = softHandStrategy(playerHandDetails, houseHand);
            break;
        case Constants.HARD_HAND:
            nextMove = hardHandStrategy(playerHandDetails, houseHand);
            break;
    }

    return nextMove;
}

function hardHandStrategy(playerHand, houseHand)
{
    let nextMove = null;

    let houseFaceUpCardValue = houseHand[0].value;

    switch (playerHand.value)
    {
        case 9:
        case 10:
            switch (houseFaceUpCardValue)
            {
                case 2:
                case 3:
                case 4:
                case 5:
                case 6:
                    nextMove = Events.DOUBLE_PLAYER;
                    break;
                default:
                    nextMove = Events.HIT_PLAYER;
                    break;
            }
            break;
        case 11:
            nextMove = Events.DOUBLE_PLAYER;
            break;
        case 12:
        case 13:
        case 14:
        case 15:
        case 16:
            switch (houseFaceUpCardValue)
            {
                case 2:
                case 3:
                case 4:
                case 5:
                case 6:
                    nextMove = Events.STAND_PLAYER;
                    break;
                default:
                    nextMove = Events.HIT_PLAYER;
                    break;
            }
            break;
        case 17:
        case 18:
        case 19:
        case 20:
        case 21:
            nextMove = Events.STAND_PLAYER;
            break;
        default:
            nextMove = Events.HIT_PLAYER;
            break;
    }

    return nextMove;
}

function softHandStrategy(playerHand, houseHand)
{
    let nextMove = null;

    switch (playerHand.value)
    {
        case 18:
        case 19:
        case 20:
        case 21:
            nextMove = Events.STAND_PLAYER;
            break;
        default:
            nextMove = Events.HIT_PLAYER;
            break;
    }

    return nextMove;
}

export { strategy }