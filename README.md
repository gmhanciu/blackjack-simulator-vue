# blackjack-simulator-vue

### Resources
[Strategy - 4 decks or more](https://www.888casino.com/blog/blackjack-strategy-guide/blackjack-charts)

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Run your tests
```
npm run test
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).